# Unity integrations for Atom
This is package provides some helpers for working with Unity

Requires installed Unity

## Features
- [ ] Unity detection
- [ ] Generating C# project files - neede for atom-omnisharp
- [ ] Unifying project versions between differnt OS

## License 
**[BSD-3-Clause](License.md)**
