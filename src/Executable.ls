require! {
    \child_process : {spawn}
}

Executable = module.exports =
    init: (@path) !->
        console.log \created @path

    call: (args) ->
        console.log "calling: #{@path} #{args.join ' '}"
        new Promise (resolve,reject) !~>
            child = spawn @path, args
            child.on \close (code) ->
                if code? and code != 0
                    reject code
                else
                    resolve!


    create: (path) ->
        Object.create @
            ..init path
