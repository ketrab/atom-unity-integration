require! {
    \path
    \atom : { CompositeDisposable}
}

sleep = (t) -> new Promise (resolve) !-> set-timeout resolve, t

var UnityFinder,fs

module.exports =
    activate: ->>
        console.log "Unity integrations activating"
        @disposables = new CompositeDisposable
            ..add atom.commands.add 'atom-workspace',
                'unity-integrations:toggle': @~toggle
                'unity-integrations:generate C# project files': @~inject
                'unity-integrations:fix-version': @~fix-version

    deactivate: ->
        console.log "Unity integrations deactivating"
        @disposables?dispose!

    fix-version: !->>
        fs ?:= require \fs-extra
        project-path = atom.project.get-paths!0
        project-version-path = path.join project-path, \ProjectSettings,\ProjectVersion.txt
        version = await fs.read-file project-version-path, \utf8
        fixed-version = version.replace \Linux '' .replace 'x' ''
        fs.write-file project-version-path, fixed-version

    toggle: !->>
        UnityFinder ?:= require \./UnityFinder
        unity = await UnityFinder.find!
        console.log "unity-integrations"
        console.log unity

    execute-script: (script-name) ->>
        UnityFinder ?:= require \./UnityFinder
        fs ?:= require \fs-extra
        project-path = atom.project.get-paths!0
        assets-path = path.join project-path,  \Assets
        editor-scripts-path = path.join assets-path, \Editor
        editor-scripts-meta-path = path.join assets-path, \Editor.meta
        script-path = path.join __dirname, \.. , \assets , script-name
        class-name = script-name.replace /.cs$/,''
        log-path = path.join project-path, \unity-log.txt
        assets-path-exists = fs.path-exists assets-path
        editor-scripts-path-exists = fs.path-exists editor-scripts-path
        unity = UnityFinder.find!
        if await assets-path-exists
            script = await fs.read-file script-path, \utf8
            tmp-script-path = path.join editor-scripts-path, script-name
            remove-editor-scripts-path = not await editor-scripts-path-exists
            await fs.output-file tmp-script-path, script
            unity = await unity
            try
                await unity.call  ['-quit', '-batchmode', '-projectPath', project-path, '-logFile', log-path, '-executeMethod' "#{class-name}.execute"]
            catch
                log = await fs.read-file log-path, \utf8
                if log.match 'Multiple Unity instances cannot open the same project.'
                    throw Error 'Multiple Unity instances cannot open the same project.'
                else
                    throw e
            finally
                remove-temp-files = [
                    fs.remove tmp-script-path
                    fs.remove log-path
                ]
                    if remove-editor-scripts-path
                        ..push do
                            fs.remove editor-scripts-path
                            fs.remove editor-scripts-meta-path
                await Promise.all remove-temp-files
        else
            throw Error "To nie jest projet Unity"

    inject: !->>
        try
            await @execute-script \GenerateVSSolution.cs
        catch
            if e.message == 'Multiple Unity instances cannot open the same project.'
                atom.notifications.add-error "Generating C# project files failed",
                    detail: "Cannot run script when Unity is running.\nExecute command again after closing Unity Editor"
                    dismissable: true
            else
                atom.notifications.add-error e.message, stack: e.stack
