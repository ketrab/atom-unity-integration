require! {
    \fs-extra : fs

    \./Executable
}

find-unity =
    windows: ->>
    linux : ->>
        try
            path-to-check = \/opt/Unity/Editor/Unity
            await fs.access path-to-check, fs.constants.X_OK
            console.log \ok
            Executable.create path-to-check
        catch
            null

choose-implementation = ->
    if find-unity[process.platform] => that
    else => -> null

module.exports =
    find: choose-implementation!
