var fs, Executable, findUnity, chooseImplementation;
fs = require('fs-extra');
Executable = require('./Executable');
findUnity = {
  windows: async function(){},
  linux: async function(){
    var pathToCheck, e;
    try {
      pathToCheck = '/opt/Unity/Editor/Unity';
      (await fs.access(pathToCheck, fs.constants.X_OK));
      console.log('ok');
      return Executable.create(pathToCheck);
    } catch (e$) {
      e = e$;
      return null;
    }
  }
};
chooseImplementation = function(){
  var that;
  if (that = findUnity[process.platform]) {
    return that;
  } else {
    return function(){
      return null;
    };
  }
};
module.exports = {
  find: chooseImplementation()
};
//# sourceMappingURL=/home/bartek/Projekty/atom/unity-integrations/lib/UnityFinder.js.map
