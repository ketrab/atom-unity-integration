var spawn, Executable;
spawn = require('child_process').spawn;
Executable = module.exports = {
  init: function(path){
    this.path = path;
    console.log('created', this.path);
  },
  call: function(args){
    var this$ = this;
    console.log("calling: " + this.path + " " + args.join(' '));
    return new Promise(function(resolve, reject){
      var child;
      child = spawn(this$.path, args);
      child.on('close', function(code){
        if (code != null && code !== 0) {
          return reject(code);
        } else {
          return resolve();
        }
      });
    });
  },
  create: function(path){
    var x$;
    x$ = Object.create(this);
    x$.init(path);
    return x$;
  }
};
//# sourceMappingURL=/home/bartek/Projekty/atom/unity-integrations/lib/Executable.js.map
