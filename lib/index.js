var path, CompositeDisposable, sleep, UnityFinder, fs;
path = require('path');
CompositeDisposable = require('atom').CompositeDisposable;
sleep = function(t){
  return new Promise(function(resolve){
    setTimeout(resolve, t);
  });
};
module.exports = {
  activate: async function(){
    var x$;
    console.log("Unity integrations activating");
    x$ = this.disposables = new CompositeDisposable;
    x$.add(atom.commands.add('atom-workspace', {
      'unity-integrations:toggle': bind$(this, 'toggle'),
      'unity-integrations:generate C# project files': bind$(this, 'inject'),
      'unity-integrations:fix-version': bind$(this, 'fixVersion')
    }));
    return x$;
  },
  deactivate: function(){
    var ref$;
    console.log("Unity integrations deactivating");
    return (ref$ = this.disposables) != null ? ref$.dispose() : void 8;
  },
  fixVersion: async function(){
    var projectPath, projectVersionPath, version, fixedVersion;
    fs == null && (fs = require('fs-extra'));
    projectPath = atom.project.getPaths()[0];
    projectVersionPath = path.join(projectPath, 'ProjectSettings', 'ProjectVersion.txt');
    version = (await fs.readFile(projectVersionPath, 'utf8'));
    fixedVersion = version.replace('Linux', '').replace('x', '');
    fs.writeFile(projectVersionPath, fixedVersion);
  },
  toggle: async function(){
    var unity;
    UnityFinder == null && (UnityFinder = require('./UnityFinder'));
    unity = (await UnityFinder.find());
    console.log("unity-integrations");
    console.log(unity);
  },
  executeScript: async function(scriptName){
    var projectPath, assetsPath, editorScriptsPath, editorScriptsMetaPath, scriptPath, className, logPath, assetsPathExists, editorScriptsPathExists, unity, script, tmpScriptPath, removeEditorScriptsPath, e, log, x$, removeTempFiles;
    UnityFinder == null && (UnityFinder = require('./UnityFinder'));
    fs == null && (fs = require('fs-extra'));
    projectPath = atom.project.getPaths()[0];
    assetsPath = path.join(projectPath, 'Assets');
    editorScriptsPath = path.join(assetsPath, 'Editor');
    editorScriptsMetaPath = path.join(assetsPath, 'Editor.meta');
    scriptPath = path.join(__dirname, '..', 'assets', scriptName);
    className = scriptName.replace(/.cs$/, '');
    logPath = path.join(projectPath, 'unity-log.txt');
    assetsPathExists = fs.pathExists(assetsPath);
    editorScriptsPathExists = fs.pathExists(editorScriptsPath);
    unity = UnityFinder.find();
    if ((await assetsPathExists)) {
      script = (await fs.readFile(scriptPath, 'utf8'));
      tmpScriptPath = path.join(editorScriptsPath, scriptName);
      removeEditorScriptsPath = !(await editorScriptsPathExists);
      (await fs.outputFile(tmpScriptPath, script));
      unity = (await unity);
      try {
        return (await unity.call(['-quit', '-batchmode', '-projectPath', projectPath, '-logFile', logPath, '-executeMethod', className + ".execute"]));
      } catch (e$) {
        e = e$;
        log = (await fs.readFile(logPath, 'utf8'));
        if (log.match('Multiple Unity instances cannot open the same project.')) {
          throw Error('Multiple Unity instances cannot open the same project.');
        } else {
          throw e;
        }
      } finally {
        x$ = removeTempFiles = [fs.remove(tmpScriptPath), fs.remove(logPath)];
        if (removeEditorScriptsPath) {
          x$.push(fs.remove(editorScriptsPath), fs.remove(editorScriptsMetaPath));
        }
        (await Promise.all(removeTempFiles));
      }
    } else {
      throw Error("To nie jest projet Unity");
    }
  },
  inject: async function(){
    var e;
    try {
      (await this.executeScript('GenerateVSSolution.cs'));
    } catch (e$) {
      e = e$;
      if (e.message === 'Multiple Unity instances cannot open the same project.') {
        atom.notifications.addError("Generating C# project files failed", {
          detail: "Cannot run script when Unity is running.\nExecute command again after closing Unity Editor",
          dismissable: true
        });
      } else {
        atom.notifications.addError(e.message, {
          stack: e.stack
        });
      }
    }
  }
};
function bind$(obj, key, target){
  return function(){ return (target || obj)[key].apply(obj, arguments) };
}
//# sourceMappingURL=/home/bartek/Projekty/atom/unity-integrations/lib/index.js.map
